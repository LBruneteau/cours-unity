# 3. Les composants de Base d'Unity

## Sommaire

[19 - Introduction aux composants d'Unity](#19---introduction-aux-composants-dunity)
[20 - Composants Physiques: Collider et Rigidbody](#20---composants-physiques-collider-et-rigidbody)
[21 - Composant Caméra (GameView)](#21---composant-caméra-gameview)
[22 - Composant Light](#22---composant-light)

## 19 - Introduction aux composants d'Unity

Les composants sont le coeur de la programmation de l'application. Ils donnent leurs propriétés et leurs comportements aux objets de la scène. Il en existe de très nombreux types sur Unity. Parmi eux le Collider donne sa forme à l'objet dans le système de collisions, et le RigidBody soumet l'objet aux lois de la physique. Pour les modifier, on se sert de la fenêtre *Inspector*. On peut ajouter des composants, éditer les propriétés de ces derniers avec différents champs et valeurs, et en programmer de sorte à ce qu'ils apparaissent de la même façon que les autres.

## 20 - Composants Physiques: Collider et Rigidbody

### Collider

Les objets 3d natifs d'Unity ont un Collider par défaut. Il définit les collisions d'un objet, et a une forme et une taille. Il est possible de le redimensionner avec le bouton *Edit Collider* dans l'inspecteur et avec les poignées vertes. Seul le collider changera, pas le rendu de l'objet. Il peut être déplacé par rapport à l'objet père. Is Trigger désactive temporairement les collisions physiques, il donnera simplemet des retours quand un objet le traverse.

### RigidBody

Au lancement du jeu les objets ne tombent pas tant qu'ils ne sont pas soumis à la gravité et aux autres lois physiques. Pour cela il faut lui ajouter un *RigidBody*. Il tomberont jusqu'à rencontrer des objets possédant un collider. Le rigidbody a plusieurs attributs. Une masse, qui n'influe pas sur la gravité mais sur les collisions entre les objets. L'attribut *Drag* représente la force des frottements de l'air, l'augmenter ralentit les mouvements de l'objet. Angular Drag est la force des frottements en cas de rotation. Si cette valeur est plus élevée alors l'objet tournera moins facilement. On peut soumettre ou non un objet à la gravité comme s'il était en apesanteur. Is Kinematic détermine si l'objet chute ou non. Constraint se change en cas de problème de collision, pour choisir un mode plus stricte mais qui consomme plus. On peut également bloquer les rotations et les mouvements sur certains axes.

## 21 - Composant Caméra (GameView)

À la création d'une nouvelle scène, Unity ajoute par défaut un composant *Main Camera*.  Ce composant possède un *Transform* et un composant de type *Camera*. Cette caméra affiche en temps réel le point de vue du joueur. On peut créer d'autres caméras. Le compsant caméra possède plusieurs attributs.

+ Le *Clear Flags* permet de choisir ce qu'affiche la caméra. Elle affiche par défaut la skybox dans le fond mais on pourrait choisir une couleur solide.

+ On peut changer la projection de la caméra de perspective à orthographique pour désactiver la profondeur. L'axe Z n'est pas rendu dans ce mode. Il est utilisé par défaut dans les projets 2d.

+ Le Field of View est la taille de l'angle de la caméra. Il permet d'élargir ou de diminuer le champ de vision.

+ La valeur Near permet de mettre un écart entre l'objet caméra et le début de l'enregistrement du champ de vision, à la manière d'un zoom. Le far est la profondeur, ou la distance de vision.

+ Le viewport rect permet de redimensionner le rendu de la caméra et de le déplacer sur l'écran. Cela peut servir pour une image dans l'image ou un écran scindé par exemple.

+ *Depth* définit la couche sur laquelle s'affiche le rendu de la caméra par rapport aux autres dans le jeu. L'augmenter permet de passer une caméra au dessus de l'autre quand on souhaite en afficher plusieurs.

+ Le *Target  Display* permet de choisir l'écran sur lequel la caméra s'affiche s'il y en a plusieurs.

## 22 - Composant Light

Par défaut, Unity crée également un objet *Directional Light*, disposant d'un *Transform* et d'n composant *Light*. Cependant il est possible d'avoir plusieurs sources de lumièr. Déplacer la source de lumière par défaut de la scène n'influe pas sur le rendu de la scène. C'est pasrce que c'est une lumière directionnelle, qui dépend de son angle de rotation. Les composants light ont différents attributs agissant sur l'éclairage de la scène.

+ La couleur de la lumière

+ Le mode permet de déterminer si la lumière est calculée en temps réel, en avance par rapport à la scène ou de mélanger.

+ L'intensité de la lumière

+ L'ombre, qui peut être désactivée, hard ou soft selon les envies, pour qu'elle soit plus ou moins visible mais aussi gourmande en ressources. Il y a également des valeurs à changer pour affecter l'ombre.

+ Le halo de lumière de la source peut être affiché.

+ Le render mode peut être changé entre *important*, *not important* et *auto*.

### Mode directional

La lumière peut être déplacée sans changements sur le rendu. Ce qui compte est son inclinaison.

### Mode spot

Crée un spot de lumière qui éclaire dans une direction précise, à la manière d'une lampe torche. On peut déplacer ce spot pour éclairer à un autre endroit, et aussi changer son inclinaison. On peut changer sa portée avec l'attribut *range*, mais aussi son intensité. L'angle de la lumière peut être élargi ou affiné pour un éclairage plus ou moins vaste.

### Mode point

La lumière dans ce mode éclaire à 360 degré, comme une torche de minecraft. Elle a également une range et une intensité.

### Mode baked

Le mode baked calcule la trajectoire de la lumière dans la scène non pas en temps réel mais en amont sur les objets sélectionnés. Il permet une bonne qualité sans coût additionnel en ressources mais ne s'adapte pas aux changements dans le jeu. Il faut pour cela sélectionner l'objet que l'on désire éclairer, aller dans le menu *Static* dans l'inspecteur et choisir *Contribute GI*. ![](./img/where_contribute_gi.png)