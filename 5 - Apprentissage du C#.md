# 5. Apprentissage du C#
## Sommaire
[34 - Notions de base : script et IDE](#34---notions-de-base--script-et-ide)
[35 - Structure d'un script C#](#35---structure-dun-script-c)
[36 - Les méthodes (Monobehaviour)](#36---les-méthodes-monobehaviour)
[37 - Les variables de base](#37---les-variables-de-base)
[38 - La portée des variables](#38---la-portée-des-variables)
[39 - Les opérations mathématiques](#39---les-opérations-mathématiques)
[40 - Les conditions](#40---les-conditions)
[41 - Opérateurs d'affectations, mathématiques, logiques, terniaires](#41---opérateurs-daffectations-mathématiques-logiques-terniaires)
[42 - Tableau simple](#42---tableau-simple)
[43 - Tableau multidimensionnel](#43---tableau-multidimensionnel)
[44 - Les boucles conditionnelles](#44---les-boucles-conditionnelles)
[45 - Les méthodes (void)](#45---les-méthodes-void)
[46 - Les méthodes (arguments)](#46---les-méthodes-arguments)
[47 - Les fonctions](#47---les-fonctions)
[48 - Conversion implicite et explicite](#48---conversion-implicite-et-explicite)
[49 - Refactoring et Debuging du code](#49---refactoring-et-debuging-du-code)

## 34 - Notions de base : script et IDE
Les scripts d'Unity sont rédigés en *C#*. Ce cours n'aborde pas la POO. Pour créer un script il faut aller dans l'onglet Projet et faire **Clic Droit &rarr; Create &rarr; C# Script**. Il faut impérativement lui donner un nom **sans espaces**. La convention de nom pour les scripts est **UneMajusculeParMot**. Ce script peut être ensuite ouvert avec l'éditeur de code favori. Pour le changer il faut aller dans **Edit &rarr; Preferences &rarr; External Tools &rarr; External Script Editor**. Les principaux sont *MonoDevelop*, *Visual Studio* et *Visual Studio Code*. On peut ensuite cliquer sur le script pour l'ouvrir.

## 35 - Structure d'un script C#
### Namespaces
Un script commence par une partie d'imports, avec `using`. On peut toujours voir
```csharp
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
```
### Classe
Une classe publique est créée par défaut. Son nom est le même que celui du fichier.
### Start
La méthode `Start` est appelée par Unity au chargement de l'objet.
### Update
La méthode `Update` est appelée une fois par frame.

Les commentaires s'écrivent avec des `\\ commentaire`. Ou `/*commentaire sur plusieurs lignes*/`. Chaque ligne finit par un point-virgule.
## 36 - Les méthodes (Monobehaviour)
Les méthodes sont privées par défaut en C#.
La méthode `Start` d'un objet est appelée avant la première mise à jour de l'objet alors que la méthode `Update` est appelée à chaque frame.
`Debug.Log()` permet d'afficher un message dans le terminal
```csharp
void Start(){
    Debug.Log("Hello World !");
}
```
Il y a d'autres méthodes qui peuvent être réécrites.
- `Awake()` est appelée à chaque démarrage.
- `OnEnable()` est appelée à l'activation du script.
- `LateUpdate()` est appelée après `Update()` à chaque frame.
- `FixedUpdate()` est appelée à un intervalle régulier de **0.02s**.
## 37 - Les variables de base
Les attributs des classes sont déclarées à l'extérieur de toutes les méthodes. La convention veut que les attributs privés s'écrivent avec une première lettre minuscule, alors que les attributs publics commencent par une majuscule. Les attributs sont privés par défaut. Les types de base sont :
- `string`
- `ìnt`
- `float`
- `bool`
### Exemple
```csharp
public class ScriptTest : MonoBehaviour
{
    string pseudo = "Squid Game";
    int elo = 20;
    float prix = 3.69f;
    bool beauMale = true;
    // Start is called before the first frame update
    void Start()
    {
        Debug.Log(pseudo);
    }
}
```


## 38 - La portée des variables
Les attributs déclarés en haut de la classe peuvent être utilisés partout dans celle-ci, alors que celles qui sont déclarées dans une méthode ne peuvent pas être appelées ailleurs.
Avant la déclaration d'une variable, elle vaut `null`.
Les variables publiques apparaissent dans l'inspecteur, et peuvent y être modifiées, ainsi que dans les autres classes. `SerializeField` permet de rendre la variable modifiable dans l'inspecteur mais pas dans les autres classes.
```csharp
[SerializeField]
private string repasPrefere = "Pâtes aux pâtes";
```
## 39 - Les opérations mathématiques
On peut concaténer des chaînes de caractères avec le `+`. On peut faire des opérations entre les `int` et les `float`.
On peut utiliser des `++`, des `--` et des `+=`.
## 40 - Les conditions
```csharp
int age = 17;

if(age < 18)
{
    Debug.Log("Tu es mineur.");
}
else if (age > 75)
{
    Debug.Log("Tu n'es pas un jeune monsieur.");
}
else
{
    Debug.Log("Tu es.");
}
```
On peut également utiliser le `switch`.
```csharp
int age = 17
switch (age)
{
    case 18:
    {
        Debug.Log("Tu viens de devenir majeur");
        break;
    }
    case 69:
    {
        Debug.Log("Nice.")
        break;
    }
    case 7:
    case 8:
    case 9:
    {
        Debug.Log("Dans un panier neuf.");
        break;
    }
    default:
    {
        Debug.Log("Tu as " + age + " ans.");
        break;
    }
}
```
## 41 - Opérateurs d'affectations, mathématiques, logiques, terniaires
L'opérateur `=` est utilisé pour l'affectation alors que `==` est utilisé pour la condition. On peut faire des vérifications avec `<`, `>`, `<=`, `>=`. La négation est faite avec le `!` avant la condition. La conjonction est `&&`. Le *ou* se fait avec `||`, le ou exclusif est `^`.
### Opérateur ternaire
```csharp
string pronom = (homme) ? "Il" : "Elle";
```
## 42 - Tableau simple
## 43 - Tableau multidimensionnel
## 44 - Les boucles conditionnelles
## 45 - Les méthodes (void)
## 46 - Les méthodes (arguments)
## 47 - Les fonctions
## 48 - Conversion implicite et explicite
## 49 - Refactoring et Debuging du code