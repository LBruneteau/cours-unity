# 2. Découverte de l'interface et des Fenêtres

## Sommaire

[7. Introduction aux scènes, aux objets et aux composants](#7-introduction-aux-scènes-aux-objets-et-aux-composants)
[8. Hierarchy (Gérer les Objets de la scène)](#8-hierarchy-gérer-les-objets-de-la-scène)
[9. Scène (Déplacer la vue dans le monde 3D)](#9-scène-déplacer-la-vue-dans-le-monde-3d)
[10. Barre d'outil de l'onglet scène](#10-barre-doutil-de-longlet-scène)
[11. Surface Snapping et Vertex Snapping](#11-surface-snapping-et-vertex-snapping)
[12. Barre outil de dimensionnement des objets](#12-barre-outil-de-dimensionnement-des-objets)
[14. Game view (Rendu du jeu)](#14-game-view-rendu-du-jeu)
[15. Fenêtre Projet](#15-fenêtre-projet)
[16. Fenêtre Inspector](#16-fenêtre-inspector)
[17. La console d'Unity](#17-la-console-dunity)
[18. Espace de travail (Layout)](#18-espace-de-travail-layout)

## 7. Introduction aux scènes, aux objets et aux composants

### Scènes

Unity permet de créer des scènes qui composent un jeu. Les scènes peuvent être des menus ou des niveaux par exemple. À partir d'une scène on peut en charger d'autres, ou simplement faire des actions dedans.

### Objets

Les scènes contiennent des objets, qui peuvent avoir différents types, par exemple modèle 3D ou élement de l'UI. Il existe aussi des objets de type terrain, pour les textures.

### Composants

Il est possible d'attacher des composants à des objets. Cela rend le développement plus modulaire. Il en existe plusieurs par défaut sur Unity, mais on peut également en créer. Les principaux sont :

- **Collider** : Détermine où rentre en collision un objet avec un autre.

- **RigidBody** : Permet de soumettre des objets à des physiques réelles et à la gravité. Il ne tombe plus à travers le terrain par exemple avec le collider.

- **Script C#** : Les scripts sont créés par les développeurs et sont rattachés aux objets comme des composants. Cela permet de manipuler les api pour intéragir avec les objets.

## 8. Hierarchy (Gérer les Objets de la scène)

![image hierarchy](./img/where_hierarchy.png)

La hierarchie se situe par défaut à gauche de l'éditeur. Il contient tous les objets présents dans la scène, sous forme d'arbre. Par défaut il y a une *Main Camera* et une *Directional Light*. On peut également s'en servir pour créer des objets via le bouton Create. On peut ensuite les renommer les supprimer les dupliquer... Il y a également une barre de recherche. Si on crée un objet depuis la scène directement, ils apparaissent dans la hiérarchie. On peut également fermer cet onglet. Pour le rouvrir, il faut aller dans le menu supérieur et faire Window → Hierarchy. La hiérarchie permet de sélectionner tous les objets de la scène.

## 9. Scène (Déplacer la vue dans le monde 3D)

![](./img/where_scene.png)

L'onglet *Scene* va contenir la représentation de la scène. Ce n'est pas la vue que le joueur aura, mais une vue globale et libre du monde créé.

### Déplacer un objet dans la vue

Lorsqu'on sélectionne un objet de la scène, via la hiérarchie ou le scène directement, trois petits sélecteurs appairaissent. Ils permettent de le déplacer dans la scène en 3D.

### Se déplacer dans la scène

#### Aller à un objet

En faisant un **Double Clic** sur un objet dans la hiérarchie, la vue de la scène se déplace immédiatement à sa position.

#### Zoomer et dézoomer

On peut zoomer et dézoomer en utilisant la **Molette** de la souris. Avec un trackpad, on peut utiliser <kbd>Alt</kbd> + **Clic Droit** et monter et descendre pour effectuer la même action.

#### Changer d'angle de caméra / pivoter

En maintenant **Clic Droit** sur la souris et en la déplaçant, on peut faire tourner la caméra, permettant ainsi d'avoir différents angles de vue.

#### Déplacer la caméra en hauteur et sur les côtés

Maintenir le **Clic Molette** et déplacer la souris permet de déplacer la caméra en deux dimensions. Elle ne peut ni avancer ni reculer, mais on peut la monter, la descendre, la bouger à gauche ou à droite. On peut aussi le faire en cliquant sur la petite icône de main et en utilisant le clic gauche.

#### Tourner autour d'un objet.

La combinaison de touche <kbd>Alt</kbd> + **Clic Gauche** permet de tourner la caméra autour de l'objet sélectionné en déplaçant la souris.

#### Se déplacer plus vite

Un mouvement effectué en maintenant la touche <kbd>Shift</kbd> est plus rapide.

#### Déplacement *Fly Through mode*

Unity a un mode de déplacement avec les touches du clavier, comme le **WASD**. Il s'active en maintenant **Clic Droit**. On peut ensuite appuyer sur les touches suivantes

+ <kbd>W</kbd> pour avancer

+ <kbd>S</kbd> pour reculer

+ <kbd>A</kbd> pour aller à gauche

+ <kbd>D</kbd> pour aller à droite

+ <kbd>E</kbd> pour monter

## 10. Barre d'outil de l'onglet scène

![](./img/barre%20outil%20scene.png)

### Changer de mode de vue

Depuis le bouton avec une forme de demi-lune, on peut changer le mode d'affichage des objets de la vue. Le mode *wireframe* permet de rendre les objets transparents et d'afficher le maillage des modèles 3D. Le mode *shaded*, le mode par défaut, affiche tout. Il existe aussi le mode *shaded wirerframe* afin de combiner les deux.  Il y a également un bouton pour passer en vue 2dn ainsi qu'un autre pour désactiver la lumière dans la scène (et non pas dans le jeu). Il y a également un menu déroulant permettant d'activer ou de désactiver le rendu de certains effets dans la scène, tels que les particules le brouillard ou la skybox. Enfin, un autre menu permet de jouer avec la taille et le nombre de dimensions des icones, afficher la grille, et désactiver l'affichage de certains gizmos, qui sont des icones et des indicateurs sur les objets.

### Boussole

![](./img/boussole.png)

Cet élement indique l'angle de vue actuel de la scène. Il tourne en même temps que la caméra. C'est un repère dans le monde 3D. On peut également s'en servir pour sélectionner un angle de vue, en cliquant dessus. Il y a également un bouton en dessous indiquant *Persp*. Cliquer dessus active le mode *Iso* et désactive les perspecitives, ce qui peut être utile dans certains scénarios.

## 11. Surface Snapping et Vertex Snapping

### Surface Snapping

En maintenant la touche <kbd>Ctrl</kbd>, (ou <kbd>Cmd</kbd> sur MacOS) on peut déplacer un objet de façon à le placer aligné à un autre. Il se déplacera cran par cran et se placera automatiquement à côté d'un autre.

### Vertex Snapping

En maintenant la touche <kbd>V</kbd> et en sélectionnant un vertex d'un objet, on peut le placer sur le vertex d'un autre objet. Cela permet de les placer côte à côte précisemment.

## 12. Barre outil de dimensionnement des objets

![](./img/outil%20dimensionnement%20objets.png)

La barre contient différents modes permettant d'intéragir avec la scène.

### View Tool

Permet de déplacer la caméra dans la scène à la manière d'un clic molette.

### Move Tool

Fait apparaître les manipulateurs permettant de déplacer les objets sur les 3 axes.

### Rotate Tool

Crée une sphère autour de l'objet sélectionné permettant de le tourner.

### Scale Tool

Permet de changer la taille d'un objet, soit entièrement avec le carré central, soit dans une dimension précise.

### Rect Tool

Permet de changer la taille d'un objet avec des petites poignées.

### Transform Tool

Affiche les manipulateurs de tous les outils précédents.

### Global / Local, Center / Pivot

Quand on selectionne plusieurs objets et que l'on cherche à les tourner, on peut le faire soit par rapport au centre de l'ensemble des objets sélectionnés, soit par rapport au pivot, le centre de chaque objet sélectionné.

Global ou local agit sur les angles de déplacement des objets. Si c'est global ce sera reglé pour augmenter le x / y / z dans le monde, alors qu'en local les manipulateurs seront tournés selon l'angle de l'objet.

## 14. Game view (Rendu du jeu)

L'onglet Game est très similaire à l'onglet Scène, et on y voit également une représentation de la scène. On peut déplacer les onglets pour les voir à côté.

### Main Camera

En cliquant sur l'objet *Main Camera*, on peut voir une preview de ce qu'elle projette. Elle représente le point de vue du joueur.

### Boutons play

Il y a des boutons en haut qui permettent de lancer le jeu à tout moment. On peut encore modifier la scène, mais tout est fonctionnel. Cependant les modifications ne seront pas enregistrées.

### Barre supérieure de l'onglet Game

#### Display

Permet de choisir l'écran sur lequel la scène s'affiche s'il y en a plusieurs.

#### Aspect

Sert à choisir le ratio de la taille de l'écran. L'utilsateur aura toujours le choix de sa résolution. On peut également créer des résolution personnalisées.

#### Scale

Permet de zoomer. Il n'y aura pas de repercussion à la sortie du jeu.

#### Maximize

Permet de lancer en plein écran.

#### Gizmos

Permet d'afficher les gizmos ou non à la manière de l'onglet Scène.

## 15. Fenêtre Projet

![](./img/fenetre%20projet.png)

L'onglet Projet contient toutes les données du jeu. On peut s'en servir pour créer plusieurs scènes et les charger. On peut également glisser déposer un fichier pour l'importer dans le projet, comme des images, des sons ou des modèles 3d. On peut ensuite les glisser sur la scène pour les ajouter. On peut créer des dossiers. On peut trier les recherches, et les enregistrer comme favorites. Pour faire un nouveau script, il faut faire Clic droit puis new C#, et lui donner un nom **sans espaces**.

## 16. Fenêtre Inspector

![](./img/where_inspector.png)

L'inspecteur est une fenêtre permettant de lire et d'éditer les propriétés des objets.

### Transform

Tous les objets disposent d'une propriété *Transform*. Il permet d'influer sur la position, la taille et la rotation des objets.

### Ajouter un composant

Pour ajouter un composant à un objet, on peut cliquer sur le bouton *Add Component* et choisir le type désiré, parmi ceux d'Unity mais aussi ceux du projet. L'objet vide ne possède qu'un composant *Transform*, mais d'autres par défaut sont plus complets. Ainsi le cube natif d'Unity est créé avec un *Transform*, un *Mesh Filter* qui est son maillage, un *Box Collider*, qui détecte les collisions et un *Mesh Renderer* servant à afficher le rendu du cube. Chaque composant a ses propriétés qui lui sont propres.

### Scripts

Les scripts C# sont des composants. On peut les ajouter à un objet en faisant un glisser-déposer depuis la fenêtre Projet vers l'inspecteur, ou en le recherchant avec le bouton *Add Component*.

On peut réinitialiser les valeurs d'un composant, ou même les copier-coller. Les composants sont également accessibles depuis le menu *Component* en haut de la fenêtre.

## 17. La console d'Unity

![](./img/where_console.png)

Unity dispose d'une console, qui permet au moteur de communiquer avec le développeur, en cas d'anomalie par exemple. On peut également s'en servir pour recevoir des informations pendant l'execution du programme. On peut trier pour n'afficher que les informations, les avertissements ou les erreurs.

##### Boutons de la console

+ **Clear** : efface tout le contenu de la console

+ **Collapse** : met sur une seule ligne toutes les mêmes erreurs afin de ne pas les avoir plein de fois à la suite.

+ **Clear on play** : fait clear à chaque lancement du jeu

+ **Error Pause** : met le jeu en pause à chaque erreur

  Le bouton *Conneected Layers* est utilisé lors de jeu en réseau.

## 18. Espace de travail (Layout)

L'espace de travail d'Unity peut être personnalisé et réarrangé. Il existe pour cela différentes dispositions pré-conçues. Ils sont disponibles dans le menu supérieur dans l'onglet **Window -> Layouts**.

+ *2 by 3* : Onglet Scène et Game l'un au dessus de l'autre

+ *4 Split* : 4 onglets scène en carré avec différents angles de vue

+ *Tall* : Scène plus grande

+ *Wide* : Scène plus large

### Personnaliser son espace de travail

Les onglets peuvent être déplacés et redimensionnés. On peut les mettre les uns sur les autres ou les séparer. On peut également les mettre dans une fenêtre à part. On peut ensuite les sauvegarder depuis **Window -> Layouts -> Save Layout**.