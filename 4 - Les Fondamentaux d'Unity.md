# 4. Les Fondamentaux d'Unity

## Sommaire

[23 - Les matériaux](#23---les-matériaux)
[24 - Les matériaux physiques](#24---les-matériaux-physiques)
[25 - Notion de parent enfant](#25---notion-de-parent-enfant)
[26 - Prefab](#26---prefab)
[30 - Asset Store](#30---asset-store)
[31 - Skybox](#31---skybox)
[32 - Modélisation de terrain 3D](#32---modélisation-de-terrain-3d)

## 23 - Les matériaux

Les matériaux permettent de changer l'aspect graphique des objets de la scène. Par défaut les formes primitives ont un *default material* dont les attributs sont grisés, impossible de les modifier. Il faut donc aller dans la fenêtre projet et faire **Clic Droit &rarr; Create &rarr; Material** pour créer un matériau modifiable. Attention à ne pas confondre avec *Physic Material*. On peut ensuite modifier ses propriétés, comme la couleur, puis affecter le matériau à un objet, en le faisant glisser sur l'objet de la scène ou dans l'inspecteur. Il y a de nombreuses propriétés permetttant de changer l'apparence du matériau. En important une image et en la glissant sur l'attribut *Normal Map*, on peut ajouter des textures au matériau, notamment avec du relief. La valeur normal map peut être changée pour faire varier le niveau de relief. 

En s'élargissant, la texture peut perdre ses proportions. Il faut changer l'attribut *Tiling* pour gérer ce comportement. Dans les paramètres de l'import de l'image on peut également choisir le wrap mode pour Repeat, Clamp, Mirror etc. 

On peut glisser des images sur des objets pour leur donner des textures, l'image se retrouvera dans l'attribut *Albedo*. On peut cependant toujours changer la couleur après.

Un objet peut avoir des shaders, mais également plusieurs matériaux 

## 24 - Les matériaux physiques

Les matériaux physiques sont différents des matériaux de base abordés précédemment. Ils servent principalement à gérer les réactions en cas de collision. Ils s'appliquent aux colliders, on peut jouer avec le *bounciness*, la friction ou l'accroche au sol. La friction à 0 se comporte plus comme sur de la glace, alors qu'à un il y a nettement plus d'accroche. On peut donner des frictions différentes selon si l'objet est immobile ou en mouvement. Les attributs *Combine* du matériau permettent de déterminer si la friction et le rebond sont calculés par rapport à la moyenne des élements à la collision, au minimum, au maximum ou au produit. 

## 25 - Notion de parent enfant

Les objets de la scène peuvent être classés sous forme de hiérarchie. On peut faire glisser un objet sur un autre dans l'onglet *Hierarchy* pour le parenter, le rendre enfant de l'autre objet. Tous les objets sont enfants de la scène qui les contient. La position des enfants est relative à celle du parent. On peut créer des objets vides et leur donner des enfants afin de créer un groupe d'objets placés les uns par rapport aux autres relativement à cet *Empty*. Faire une rotation d'un parent fait également tourner ses enfants. L'échelle est également relative au parent. 

## 26 - Prefab

Un préfab est un ensemble d'élements qui va être réutilisable dans la même scène, dans une autre scène et même dans un autre jeu. Pour cela on peut faire un clic droit dans l'onglet projet et faire **Create &rarr; Prefab**. On peut faire glisser un objet et sa hiérarchie entière sur le préfab pour lui affecter. On peut également simplement faire glisser l'objet de la hiérarchie au projet. Les prefabs apparaissent en bleu dans la hiérarchie. Tous les enfants et attributs du préfab sont conservés, ainsi que leur valeur. On peut ensuite glisser déposer le préfab dans une scène pour recréer un objet similaire. Changer cet objet n'impactera pas le préfab. Il y a un menu déroulant *Overrides* qui affiche toutes les modifications faites par rapport au préfab de base, permettant de les appliquer ou de les annuler. Pour ne plus qu'un objet de la scène ne soit relié à un préfab, il faut faire **GameObject &rarr; Break Prefab Instance**. L'objet deviendra indépendant du préfab qui l'a créé. Un préfab peut être composé d'autres préfabs. 

### Mode Préfab

En cliquant sur un préfab, on aperçoit dans l'inspecteur l'ensemble de ses propriétés. On peut les modifier pour affecter directement le préfab. Il y a un bouton auto-save, il faut donc être prudent ou le décocher. Les changements affectent tous les objets associés à ce préfab.

### Préfabs variants

Les préfabs variants fonctionnent à la manière de l'héritage en programmation orientée objet. Pour cela **Clic Droit sur un Prefab &rarr; Create &rarr; Prefab Variant**. On peut ensuite changer des propriétés des parents, et elles se répercuteront sur les préfabs enfants, qui peuvent avoir des modifications indépendantes. Si on modifie une propriété d'un enfant, il est déshérité sur cette propriété et ne suivra plus les valeurs des parents. Il est cependant possible avec l'override de restaurer l'héritage. 

## 30 - Asset Store

Unity dispose d'un *Asset Store*, qui permet de télécharger du contenu gratuit ou payant à intégrer dans le jeu. Un compte Unity est nécessaire pour l'utiliser. On peut y trouver des sons, des modèles 3d, des textures, des scripts et bien plus encore. On peut ouvrir un onglet vers l'Asset Store depuis le menu *Window* en haut de l'écran. On peut retrouver ses anciens téléchargements à l'aide d'un bouton en haut de l'écran. Il y a des catégories et des sous catégories pour mieux rechercher. On peut télécharger et importer des assets facilement. 

### Standard Assets

Unity dispose de *Standard Assets*, une collection d'assets de base créée par Unity. Ils contiennent de nombreux assets très utiles aux développeurs de base comme des sols, des murs, et même un *FPSController* permettant d'avoir rapidement une vue à la première personne pour le joueur. Les standard assets sont disponibles dans l'Asset Store, cependant ils ne sont plus maintenus depuis 2018. Il faudra donc corriger manuellement les quelques erreurs dues à l'ancienneté du pack.

## 31 - Skybox

Unity permet de changer de la skybox par défaut, en en créant ou en en important. Une skybox est un matériau. Au lancement de la scène, le joueur est placé au centre de la skybox. Elle a donc 6 faces, car elle se présente sous forme de cube géant. On peut donc créer une skybox à l'aide d'un matériau à 6 faces, présenté comme un patron de cube. On peut également prendre une image panoramique et la convertir en cylindre sur Unity. Pour créer une skybox, il faut d'abord faire un nouveau material. Sur le menu déroulant *Shader*, il faut choisir *Skybox*. On dispose ensuite de 3 options

### 6-sided

Permet de sélectionner 6 textures pour chaque face du cube. Il faut choisir une texture par côté.

### Cubemap

Image à 360° pour le cylindre. Il faut importer une image, et dans ses paramètres d'import accessibles en cliquant sur l'asset, il faut mettre *Texture Shape* à *Cube*. Il faut ensuite passer *Mapping* à *Latitude - Longitude Layout (Cylindric)* pour avoir l'effet désiré. Enfin il faut appliquer les modifications. Pour l'ajouter à la skybox il faut faire glisser le matériau obtenu sur l'attribut *Cubemap* de la skybox.

### Procedural

La skybox par défaut mais modifiable avec de nombreux paramètres.

Pour appliquer une skybox, il faut faire **Window → Lightning → Settings** puis prendre le materiau créé dans *Skybox Material*.

## 32 - Modélisation de terrain 3D

Unity n'est pas un logiciel de modélisation, cependant il est possible d'y créer des terrains grâce à un outil. 

Pour créer un terrain vierge, on peut faire **GameObject &rarr; 3D Object &rarr; Terrain**. Le terrain créé est très grand, notamment à l'échelle des personnages des standard assets. Un nouveau terrain est créé dans l'onglet *Projet*.

Un terrain est composé d'un composant *Terrain* et d'un composant *Terrain Collider*. 

### Le composant Terrain

Il dispose de plusieurs petits boutons permettant d'ajuster des paramètres

#### Paint Terrain

Cet outil permet, à l'aide d'un pinceau de taille et d'opacité variable, de monter et de descendre le niveau du terrain avec *raise or lower terrain*, d'appliquer différentes textures par section avec *paint texture*, de fixer une hauteur de terrain avec *Set Height*, de choisir la forme de la brosse afin de mieux modéliser. On peut créer ses propres brosses avec des textures. On peut ajouter des textures sous la forme de layers afin de les appliquer au terrain à différents endroits

#### Paint Trees

Il permet de choisir un préfab représentant un arbre et de les placer de manière plus ou moins dense sur le terrain. 

#### Paint Details

Cet outil permet d'importer une texture d'herbe afin de la rajouter petit à petit sur le terrain. 

#### Create Neighbour Terrains

Permet de placer simplement des terrains les uns à côté des autres

#### Terrain Settings

Ce bouton permet d'accéder à de nombreux paramètres du terrain. On peut y changer sa taille dans la catégorie *Resolution*, mais aussi régler de nombreux autres paramètres sur l'optimisation et l'affichage, car les terrains peuvent se montrer gourmands en ressources. Cocher *Draw Intensed* allège considérablement la charge du processeur. 