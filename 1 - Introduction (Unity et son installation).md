# 1. Introduction (Unity et son installation)

## Sommaire

[2. Les différentes versions  d'Unity](#2.-Les-différentes-versions-d'Unity)
[3. Installation d'Unity en détails](#3.-Installation-d'Unity-en-détails)
[4. Unity Hub](#4-unity-hub)
[6. Premier lancement d'Unity](#6-premier-lancement-dunity)

## 2. Les différentes versions d'Unity

### Site d'Unity

https://unity3d.com/fr

### Différences entre les versions

Les versions ont des différences assez mineures pour la plupart des utilisateurs. Les changements sont principalement perçus par les équipe de développeurs avancés.

### Différences entre les offres

Il existe trois offres Unity. La *Personal*, la *Plus* et la *Pro*. Chaque version dispose de toutes les fonctionnalités.

#### Personal - gratuit

Principalement pour des étudiants et des développeurs indépendants. Il n'y a aucun avantage mais toutes les fonctionnalités d'Unity sont disponibles. Une licence payant sera nécessaire si le jeu rapporte plus de **100 000 $**.

#### Plus - 32€/mois

Pour des créateurs plus avancés. Contient des cours, un générateur de terrain et de cours, et des avantages dans l'Asset Store. Utilisable pour des jeux rapportant jusqu'à **200 000 $**.

#### Pro - 115€/mois

Pour des équipes de développeurs sur des gros projets. Contient les avantages Plus ainsi que quelques autres. On peut s'en servir **sans limite de bénéfices**.

### Installation d'Unity

Depuis le site, une fois un tarif choisi, on peut télécharger soit Unity soit Unity Hub, servant à gérer les versions d'Unity. 

## 3. Installation d'Unity en détails

Cette section concerne la version directe de l'installation d'Unity. Depuis le [Site d'Unity](https://unity3d.com/fr), on peut télécharger l'installateur d'une version spécifique du moteur. On peut ensuite choisir de télécharger avec des packs de contenu optionnel avant de lancer l'installation. 

## 4. Unity Hub

Depuis le site d'Unity, il est également possible d'installer *Unity Hub*, un outil pour gérer différentes versions d'Unity.

### Onglet Projets

 Il est possible de choisir un projet à lancer, depuis le disque ou le *cloud*.  On peut en créer ou en ouvrir depuis le disque. Il est possible de changer un projet de version. 

### Onglet Learn

Contient différent tutoriels d'Unity afin de débuter avec le moteur. Ils sont en anglais, et nécessitent déja une certaine connaissance du C#. Il y a également des ressources, telles qu'un pack de particules.

### Onglet Installs

Il permet de gérer les différentes versions du moteur. Il indique toutes les versions installées. On peut également localiser une version déja installée pour l'ajouter au hub.  On peut ajouter une version comme préférée. Depuis l'onglet *Official Releases*, il est possible d'installer de nouvelles versions d'Unity. On a également accès aux versions beta. On peut également installer des composants pour chaque version, comme par exemple des supports pour la compilation. 

### Autres versions

Unity Hub ne dispose pas de toutes les versions d'Unity. Il est donc parfois nécessaire pour utiliser une version spécifique d'alller sur le  [site des archives de téléchargement Unity](https://unity3d.com/fr/get-unity/download/archive). On peut y installer toutes les versions que l'on désire, même des plus anciennes. Il faudra ensuite ajouter la version sur Unity Hub.

## 6. Premier lancement d'Unity

Il est intéressant de se connecter avec un compte Unity pour accéder à certaines fonctionnalités, comme le développement en équipe. Cela donne également le droit de télécharger depuis l'Asset Store d'Unity. 

### Créer un projet

Il sera demandé, en cliquant sur le bouton *New*, de donner un nom pour le projet. Il faut également choisir une localisation pour le dossier. Le template est le format du projet, 2D, 3D, plus léger ou plus lourd. On peut ajouter des packages déjà installés depuis l'Asset Store afin de les integrer au projet. À la création Unity se lance. 